<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8"/>
		<title>Home</title>
		<link rel="stylesheet" href="style.css"/>
	</head>

	<body>
		
		<?php include 'navbar.php';?>
		
		<div class="content">
		
			<div class="heading">
				<h2>Welcome to St Catherine's Church and Centre, a vibrant, open and inclusive church and community centre in the heart of Wakefield, West Yorkshire</h2>
			</div>
			
			<div class="socialmedia">
			    <p><img src="images/facebook.svg" width="37.5" height="37.5"> <img src="images/twitter.svg" width="37.5" height="30.5"></p>
			</div>
			
			<div class="maininfo">
				<p>St Catherine's is a diverse community with a rich life of worship, prayer and community engagement. We seek through our church community activities and pastoral care to make the Christian faith relevant and accessible.</p>

				<p>Our ministry is rooted in the care of the vulnerable and disadvantaged and our purpose-built, modern community centre provides a range of services including the Friendship Community Café, Day Care Services for the elderly, conference/meeting facilities for local community groups and businesses and other activities designed to help alleviate poverty and promote social justice.</p>

				<p>In addition to our regular church services, the Centre is open every week day from 8am - 5pm.  The Friendship Community Café is also open every week day from 8am to 3pm.</p>
			</div>
			
			<div class="images">
			    <p><b>St Catherine's are hard at work delivering food to local people, families and other food hubs. We would like to thank everyone involved in these trying times. For more information on our activities and services for the local community, please click the above links.</b></p>
			    <img src="images/Food.drop.1.jpg" width="240" height="320">
			    <img src="images/Food.drop.2.jpg" width="240" height="320">
			    <img src="images/Food.drop.3.jpg" width="240" height="320">
			    <img src="images/Food.drop.4.jpg" width="240" height="320">
			    <img src="images/Food.drop.5.jpg" width="240" height="320">
			    <img src="images/Food.drop.7.jpg" width="440" height="320">
			    <br>
			</div>
		</div>
	
	</body>


</html>