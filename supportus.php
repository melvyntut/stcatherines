<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8"/>
		<title>Support Us</title>
		<link rel="stylesheet" href="style.css"/>
	</head>

	<body>
	
		<?php include 'navbar.php';?>
		
		<div class="content">
			<div class="heading">
				<h2>There are many ways you can support the work we do at St Catherine's Church and Centre</h2>
			</div>
			
			<div class="info">
				<p>Every penny of profit we generate is reinvested back into our services and in developing our provision:</p>
				<br>
				<ol>
					<li>We provide services to a diverse and often deprived community.</li>
					<li>We support those in dire need with direct and practical help.</li>
					<li>We design activities and services which are designed to promote community cohesion and break down barriers between ethnic groups.</li>
					<li>We assist jobseekers back into work and provide links to sources of advice and information on benefits and debt management.</li>
					<li>We help keep older people involved with life and living independently; whilst also supporting carers and providing them with support and respite.</li>
				</ol>
				<br>
				<p><b>Donate to the Work of our Emergency Food Store</b> - by clicking on the donate button which you will find on this page and donating via PayPal.</p>
				<br>
				<p><b>Use the Centre/Friendship Café</b> - Simply by booking our conferencing/meeting rooms or using the Friendship Community Café, you are directly supporting our work.</p>
				<br>
				<p><b>Volunteer</b> - Individuals, teams or entire companies can donate time and energy to a good cause whilst gaining new skills and meeting new people. We have a range of volunteer opportunities available, depending on your interests. These include: admin support, fundraising, befriending and supporting older people, helping with community centre activities and bookings, general caretaking of the building. Volunteers can also access our free Job Club to assist them in updating their CVs with details of their volunteering experience. For more details of these opportunities, please contact Lisa Grant, Centre Manager on 01924 211130 or lisa@stcatherines-wakefield.org.uk</p>
				<br>
				<p><b>CSR Projects</b> - We have a range of opportunities for projects or partnerships which will tick your CSR box. We will plan with you a project which ensures that both parties' needs are met and that the results are beneficial to local residents long in the future. Past projects have included Painting and Decorating, Gardening, Marketing, Youth Projects, Publicity Campaigns and volunteering to deliver events with older adults. Please contact Lisa Grant, Centre Manager on 01924 211130 or lisa@stcatherines-wakefield.org.uk</p>
			</div>
		</div>
		
	</body>


</html>