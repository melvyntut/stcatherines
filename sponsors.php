<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8"/>
		<title>Sponsors</title>
		<link rel="stylesheet" href="style.css"/>
	</head>

	<body>
	
		<?php include 'navbar.php';?>
		
		<div class="content">
			<div class="heading">
				<h2>Sponsors</h2>
			</div>
			
			<div class="info">
				<p>We are incredibly grateful for the support we currently receive from a number of businesses:</p>
				<br>
				<p><a href="https://www.ahc.com">AHC</a></p>
				<br>
				<p><a href="https://www.allumsdirect.co.uk">Allums Butchers</a></p>
				<br>
				<p><a href="https://www.greggs.co.uk">Greggs</a></p>
				<br>
				<p><a href="https://www.sainsburys.co.uk">Sainsbury's</a></p>
				<br>
				<p><a href="https://www.symingtons.com">Symington's</a></p>
			</div>
		</div>
		
	</body>


</html>