<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8"/>
		<title>Contact</title>
		<link rel="stylesheet" href="style.css"/>
	</head>

	<body>
	
		<?php include 'navbar.php';?>
		
		<div class="content">
			<div class="heading">
				<h2>Contact</h2>
			</div>
		
			<div class="info">
				<p><b>Find Us:</b> St Catherine's Church and Centre is located on the Doncaster Road in Wakefield. Coming into Wakefield on the A638 Doncaster Road you will find us on the left-hand side just after Belle Vue (Mobile Rocket Stadium).</p>
				<br>
				<iframe src="https://www.google.com/maps/embed?pb=!1m17!1m11!1m3!1d1387.166359286144!2d-1.479874067855294!3d53.67095342015806!2m2!1f0!2f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4879665b273cf18f%3A0xccc1b645469f9909!2sSt.%20Catherine&#39;s%20C%20of%20E%20Church!5e1!3m2!1sen!2suk!4v1590845057219!5m2!1sen!2suk" width="300" height="225" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
				<br>
				<br>
				<p><b>Centre Manager</b> - <b>Lisa Grant</b>: lisa@stcatherines-wakefield.org.uk</p>
				<br>
				<p><b>Catering Manager</b> - <b>Frank Robinson</b>: catering@stcatherines-wakefield.org.uk</p>
				<br>
				<p><b>Daycare Manager</b> - <b>Tracey Purchon</b>: socialcare@stcatherines-wakefield.org.uk</p>
			</div>
		</div>
		
		
		
	</body>


</html>