<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8"/>
		<title>Worship & Prayer</title>
		<link rel="stylesheet" href="style.css"/>
	</head>

	<body>
	
		<?php include 'navbar.php';?>
		
		<div class="content">
			<div class="heading">
				<h2>Worship and prayer are at the heart of all that we do</h2>
			</div>
			<div class="info">
				<p>We offer a variety of prayer and worship opportunities throughout the week, including:</p>
				<br>
				<p>Monday (8:30am - 9:00am): Morning Prayer</p>
				<br>
				<p>Wednesday (9:30am - 10:00am): Holy Communion</p>
				<br>
				<p>Sunday (10:30am - 11:45am): Worship for All (with Children's Church)</p>
				<br>
				<p>You are always welcome to use the Chapel area for private prayer. Please bear in mind that, because of other activities, the Chapel area may occasionally be in use. However, we also have a dedicated prayer room available for use at all times.</p>
				<br>
				<p>For more information, contact the <b>vicar Revd David Gerrard</b> on <b>01924 211130 (church)</b></p>
			</div>
		</div>
	
	</body>


</html>