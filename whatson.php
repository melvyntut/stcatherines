<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8"/>
		<title>What's On?</title>
		<link rel="stylesheet" href="style.css"/>
	</head>

	<body>
	
		<?php include 'navbar.php';?>
		
		<div class="content">
			<div class="heading">
				<h2>What's On?</h2>
			</div>
		
			<div class="info">
				<p><b>Friendship Café</b></p>
				<br>
					<p>Monday - Friday 8am to 3pm</p>
					<br>
					<p>Come and join us in our café for a chance to meet new people, try some of our delicious homemade cakes or hot meals.</p>
					<br>
				<p><b>The Monday Club</b></p>
					<br>
					<p>The club meets every other Monday from 4.00pm - 6.00pm.</p>
					<br>
					<p>The group enjoys a range of activities at their meetings and regularly arranges day trips and guest speakers. The club allows people to develop new friendships, enjoy the company of others and learn about new interests and enthusiasms.</p>
					<br>
					<p>New members are always welcome.</p>
					<br>
				<p><b>Art & Crafts Club</b></p>
					<br>
					<p>Tuesday 10am - 12pm. Free session offering a range of art and craft activities.</p>
					<br>
				<p><b>Meet and Eat</b></p>
					<br>
					<p>A free hot meal for vulnerable people in our community. Every Tuesday from 6:30pm.</p>
					<br>
				<p><b>Gardening Club</b></p>
					<br>
					<p>Every other Wednesday 10am - 12:30pm. Free club for novice and experienced gardeners alike. Links to local allotments.</p>
					<br>
				<p><b>The Recovery Café</b></p>
					<br>
					<p>Thursdays 3pm - 6pm. Mental health support group providing a chance to meet and share a two-course meal for only £2.00.</p>
					<br>
				<p><b>Creative Writing</b>
					<br>
					<p>Every Thursday from 3pm - 5pm. Free session open to all abilities.</p>
					<br>
				<p><b>Community Singing Group</b></p>
					<br>
					<p>Every Thursday 6:30pm - 8pm. Free choir and music/singing group. All abilities welcome.</p>
					<br>
				<p><b>Wellbeing Wednesday</b></p>
					<br>
					<p>On the last Wednesday of each month</p>
					<br>
					<p>A lunchtime quiz session which offers a fun way to exercise your general knowledge brain and relax with friends. Come on your own and join one of our friendly teams or bring your own team. The session includes quizzes, puzzles and prizes. From 12:30pm - 2:30pm. £1 per person.</p>
			</div>
		</div>
		
	</body>


</html>