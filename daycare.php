<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8"/>
		<title>Day Care</title>
		<link rel="stylesheet" href="style.css"/>
	</head>

	<body>
	
		<?php include 'navbar.php';?>
		
		<div class="content">
			<div class="heading">
				<h2>St Catherine's Church Day Care Service is a highly regarded, quality day care provision</h2>
			</div>
			
			<div class="info">
				<p>The centre is a modern, fully equipped venue with a range of special adaptations to ensure we can provide full catering for all our clients. We pride ourselves on a service which is second to none and is surprisingly affordable. All our staff are fully qualified and highly experienced.</p>
				<br>
				<p><b>Our Mission</b></p>
				<p>To ensure that our service provides fun, friendship and a personal touch. We give our clients one-to-one attention and respect. We don't work with numbers, we work with people and their families.</p>
				<br>
				<p><b>About the Service</b></p>
				<p>The service runs Monday to Friday from 9am to 4pm. The service includes door-to-door transport to and from the service. A two-course nutritionally balanced lunch and refreshments are available throughout the day. We also provide a huge range of activities for our clients, including: professional entertainers, craft activities, armchair exercises, day trips, seasonal projects, reminiscing, hand massages, and much more. We ensure that we personalise our service to take into account each individual client's requirements and life experiences.</p>
				<br>
				<p><b>Charges</b><p>
				<p>£30 per person per full day. The charge includes transport, lunch, refreshments and activities. You may be eligible for a subsidised place. Please ask for further details.</p>
				<br>
				<p>Please contact the <b>Daycare Manager, Tracey Purchon</b> on <b>01924 211130</b> to discuss how you could benefit from the services we provide, or send an email to socialcare@stcatherines-wakefield.org.uk</p>
			</div>
		</div>
		
	</body>


</html>