# Website

These are the files to be used for the website.

## Installation

Before installing these files, you should first install a program called Xampp. It will allow you to run PHP code locally on your computer. It can be downloaded at https://www.apachefriends.org

## Usage

Once Xampp is installed, install the website files inside Xampp's htdocs folder. Run the Xampp control panel and click the "start" button next to Apache. Go to your browser and type in localhost/[name of folder where the site files are installed] and the website should run.