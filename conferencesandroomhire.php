<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8"/>
		<title>Conferences & Room Hire</title>
		<link rel="stylesheet" href="style.css"/>
	</head>

	<body>
	
		<?php include 'navbar.php';?>
		
		<div class="content">
			<div class="heading">
				<h2>St Catherine's Church & Centre offers a perfect venue for conferences, training and other special events</h2>
			</div>
			
			<div class="info">
				<p>Whether for a major conference, training days, or one-on-one interviews, we have a range of rooms available for hire for groups from 2 to 200! Our dedicated staff are on-hand to cater for all your needs and to ensure your event goes well.</p>
				<br>
				<p>Our main auditorium has a seating capacity of 200 people (theatre style) with plasma screens, audio equipment and induction loop. In addition, we have a variety of other rooms available for training, meetings and special events with:</p>
				<br>
				<ul>
					<li>TV, video, DVD, CD and PC equipment</li>
					<li>Wireless broadband access</li>
					<li>Flipcharts</li>
					<li>Projectors and screens</li>
					<li>Office backup, e.g. photocopying, faxing etc.</li>
				</ul>
				<br>
				<p>Whatever your event or special occasion, we can provide a catering package to suit your needs. We also have rooms availabe for longer term lets. Special rates are available for community groups and charities.</p>
				<br>
				<p><b>For more information, contact Lisa Grant on 01924 211130</b> or by email.</p>
				<br>
				<p>Alternatively, to download our comprehensive brochure (pdf), click here.</p>
				<br>
				<p><b>St Catherine's is a charity dedicated to investing in services and activities that support some of the most vulnerable adults and young children in our region. Every penny we make from our commercial activities is reinvested back into the charitable work of the organisation. By using us for your event, you are directly contributing to the health and wellbeing of our service users. Thank you for helping us to help others.</b></p>
			</div>
		</div>
		
	</body>


</html>