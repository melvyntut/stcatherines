<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8"/>
		<title>Community Activities</title>
		<link rel="stylesheet" href="style.css"/>
	</head>

	<body>
	
		<?php include 'navbar.php';?>
		
		<div class="content">
			<div class="heading">
				<h2>About our Community Activities</h2>
			</div>
			
			<div class="info">
				<p>At St Catherine's, serving the local community is fundamental to what we do. We aim to design activities and projects which meet the needs of local people.</p>
				<br>
				<p>All the profits from our conferencing, room hire and catering departments are reinvested back into the delivery of a range of innovative services and activities designed to ensure community involvement and improve the day-to-day wellbeing, health and quality of life of service users and community members.</p>
				<br>
				<p><b>Please click on 'What's On?' to see details of our regular activites.</b></p>
			</div>
		</div>
		
	</body>


</html>